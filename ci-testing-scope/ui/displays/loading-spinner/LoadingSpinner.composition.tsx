import React from "react";
import { LoadingSpinner } from "./LoadingSpinner";

export const BasicLoadingSpinner = () => (
    <LoadingSpinner />
);

export const LoadingSpinnerWithColor = () => (
    <LoadingSpinner color="success" />
);

export const LoadingSpinnerWithSize = () => (
    <LoadingSpinner size={40} />
);
