import { CircularProgress, Grid, Theme } from "@mui/material";
import { SxProps } from "@mui/system";
import React from "react";

export interface ILoadingSpinnerProps {
  /**
   * The color of the spinner component - uses Material UI theme values.
   */
  readonly color?:
    | "primary"
    | "secondary"
    | "error"
    | "info"
    | "success"
    | "warning"
    | "inherit";

  /**
   * The size of the spinner, in pixels
   */
  readonly size?: number;

  /**
   * Custom styles for the component wrapping the spinner
   */
  readonly wrapperStyles?: SxProps<Theme>;
}

export const LoadingSpinner = ({
  color = "primary",
  size = 20,
  wrapperStyles,
}: ILoadingSpinnerProps) => {
  return (
    <Grid
      alignItems="center"
      container
      justifyContent="center"
      minHeight={size * 1.5}
      sx={wrapperStyles}
    >
      <CircularProgress
        color={color}
        data-testid="loading-spinner"
        size={size}
      />
    </Grid>
  );
};
