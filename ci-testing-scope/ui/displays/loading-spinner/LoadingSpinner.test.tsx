import { render, waitFor } from "@testing-library/react";
import * as React from "react";
import { BasicLoadingSpinner } from "./LoadingSpinner.composition";

it("should render spinner exactly once", async () => {
  const { getAllByTestId, getByTestId } = render(<BasicLoadingSpinner />);

  const spinners = getAllByTestId("loading-spinner");

  expect(spinners.length).toEqual(1);
});
