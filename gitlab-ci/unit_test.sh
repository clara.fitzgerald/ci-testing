#!/bin/bash
set -e

bit install
bit compile
bit check-types --strict
bit build
npm install
npm run lint
