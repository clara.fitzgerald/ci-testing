#!/bin/bash
set -e


###
# Git Setup
###
git config --global user.name "clara.fitzgerald"
git config --global user.email "clara.fitzgerald@yoello.com"


###
# Bit Setup
###
bit config set user.token "$BIT_TOKEN"
bit install
bit compile

###
# Bit Export
# We only export components that have been soft tagged by developers
# Use bit tag [id...] --soft before making your MR
###
bit tag --all
bit export

